<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
    <head>
        <title>Gualmarsh / Home</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="style">
        <link rel="stylesheet" href="css-font">
    </head>
    <body>
        <div class="wrapper d-flex align-items-stretch" style="font-family: 'Bogle';">
            <nav id="sidebar">
                <div class="custom-menu">
                </div>
                <div class="p-4">
                    <img src="logo" width="275" height="65" alt="Logo"/> 
                    <ul class="list-unstyled components mb-5">
                        <li class="active">
                            <a href="<%=request.getContextPath()%>/home"><span class="fa fa-home mr-3"></span> Home</a>
                        </li>
                        <li>
                            <a href="<%=request.getContextPath()%>/charts"><span class="fa fa-bar-chart mr-3"></span> Charts</a>
                        </li>
                        <li>
                            <a href="<%=request.getContextPath()%>/employees"><span class="fa fa-users mr-3"></span> Employees</a>
                        </li>
                        <li>
                            <a href="<%=request.getContextPath()%>/customers"><span class="fa fa-user-o mr-3"></span> Customers</a>
                        </li>
                        <li>
                            <a href="<%=request.getContextPath()%>/products"><span class="fa fa-shopping-cart mr-3"></span> Products</a>
                        </li>
                        <li>
                            <a href="<%=request.getContextPath()%>/sale"><span class="fa fa-usd mr-3"></span> New Sale</a>
                        </li>
                        <li>
                            <a href="<%=request.getContextPath()%>/report-list"><span class="fa fa-file-text mr-3"></span> Reports</a>
                        </li>
                    </ul>
                    <div class="footer">
                        <p>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;
                            <script>document.write(new Date().getFullYear());</script> All rights reserved | 
                            <i class="icon-heart" aria-hidden="true"></i>GADAFA <a
                                href="https://fidelitas.com" target="_blank"></a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>

                </div>
            </nav>
 
            <div id="content" class="p-4 p-md-5 pt-5">
                <div class="containericon" style="top: 100px; left: 400px">
                    <img src="employeeicon" alt="Employee" class="image" style="top: 100px; left: 400px; width: 100%;"/>
                    <div class="middle" style="  top: 115%; left: 50%;">
                        <div> 
                            <a href="<%=request.getContextPath()%>/employees" class="btn btn-success">Employees</a>
                        </div>                       
                    </div>
                </div>
                <div class="containericon" style="top: 100px; left: 750px">
                    <img src="customericon" alt="Customer" class="image" style="top: 100px; left: 750px; width: 100%;"/>
                    <div class="middle" style="  top: 115%; left: 50%;">
                        <div> 
                            <a href="<%=request.getContextPath()%>/customers" class="btn btn-success">Customers</a>
                        </div>                       
                    </div>
                </div>
                <div class="containericon" style="top: 100px; left: 1100px">
                    <img src="producticon" alt="Product" class="image" style="top: 100px; left: 1100px; width: 100%;"/>
                    <div class="middle" style="  top: 115%; left: 50%;">
                        <div> 
                            <a href="<%=request.getContextPath()%>/products" class="btn btn-success">Products</a>
                        </div>                       
                    </div>
                </div>
                <div class="containericon" style="top: 100px; left: 1450px">
                    <img src="saleicon" alt="Sale" class="image" style="top: 100px; left: 1450px; width: 100%;"/>
                    <div class="middle" style="  top: 115%; left: 50%;">
                        <div> 
                            <a href="<%=request.getContextPath()%>/sale" class="btn btn-success">Sale</a>
                        </div>                       
                    </div>
                </div>
                <div class="containericon" style="top: 500px; left: 400px">
                    <img src="reporticon" alt="Report" class="image" style="top: 500px; left: 400px; width: 100%;"/>
                    <div class="middle" style="  top: 115%; left: 50%;">
                        <div> 
                            <a href="<%=request.getContextPath()%>/report-list" class="btn btn-success">Reports</a>
                        </div>                       
                    </div>
                </div>
                <div class="containericon" style="top: 500px; left: 750px">
                    <img src="profileicon" alt="Profile" class="image" style="top: 500px; left: 750px; width: 100%;"/>
                    <div class="middle" style="  top: 115%; left: 50%;">
                        <div> 
                            <a href="<%=request.getContextPath()%>/profile" class="btn btn-success">Profile</a>
                        </div>                       
                    </div>
                </div>
                <div class="containericon" style="top: 500px; left: 1100px">
                    <img src="graphicon" alt="Charts" class="image" style="top: 500px; left: 1100px; width: 100%;"/>
                    <div class="middle" style="  top: 115%; left: 50%;">
                        <div> 
                            <a href="<%=request.getContextPath()%>/charts" class="btn btn-success">Charts</a>
                        </div>                       
                    </div>
                </div>
                <div class="containericon" style="top: 500px; left: 1450px">
                    <img src="logouticon" alt="Logout" class="image" style="top: 500px; left: 1450px; width: 100%;"/>
                    <div class="middle" style="  top: 115%; left: 50%;">
                        <div> 
                            <a href="<%=request.getContextPath()%>/login" class="btn btn-success">Logout</a>
                        </div>                       
                    </div>
                </div>
            </div>   

        </div>
        <script src="jquery"></script>
        <script src="popper"></script>
        <script src="bootstrap"></script>
        <script src="main"></script>
    </body>

</html>
