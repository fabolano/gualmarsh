<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
    <head>
        <title>Gualmarsh / Home</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link rel="stylesheet" href="style">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css-font">
        <script>
            function displayStuff() {
            var e = document.getElementById('selecter');
            var val = e.options[e.selectedIndex].value;

            switch (val) {
                case "Camisa":
                    document.getElementById('one').style.display = 'block';
                    break;
                case "2":
                    document.getElementById('two').style.display = 'block';
                    break;
                }
            }
    
         </script>
    </head>
    <body>
        <div class="wrapper d-flex align-items-stretch" style="font-family: 'Bogle';">
            <nav id="sidebar">
                <div class="custom-menu">
                </div>
                <div class="p-4">
                    <img src="logo" width="275" height="65" alt="Logo"/> 
                    <ul class="list-unstyled components mb-5">
                        <li class="active">
                            <a href="<%=request.getContextPath()%>/home"><span class="fa fa-home mr-3"></span> Home</a>
                        </li>
                        <li>
                            <a href="<%=request.getContextPath()%>/charts"><span class="fa fa-bar-chart mr-3"></span> Charts</a>
                        </li>
                        <li>
                            <a href="<%=request.getContextPath()%>/employees"><span class="fa fa-users mr-3"></span> Employees</a>
                        </li>
                        <li>
                            <a href="<%=request.getContextPath()%>/customers"><span class="fa fa-user-o mr-3"></span> Customers</a>
                        </li>
                        <li>
                            <a href="<%=request.getContextPath()%>/products"><span class="fa fa-shopping-cart mr-3"></span> Products</a>
                        </li>
                        <li>
                            <a href="<%=request.getContextPath()%>/sale"><span class="fa fa-usd mr-3"></span> New Sale</a>
                        </li>
                        <li>
                            <a href="<%=request.getContextPath()%>/report-list"><span class="fa fa-file-text mr-3"></span> Reports</a>
                        </li>
                    </ul>
                    <div class="footer">
                        <p>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;
                           <script>document.write(new Date().getFullYear());</script> All rights reserved | 
                            <i class="icon-heart" aria-hidden="true"></i>GADAFA <a
                                href="https://fidelitas.com" target="_blank"></a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>

                </div>
            </nav>

            <div id="content" class="container col-md-5 pt-5">
                <div class="card">
                    <div class="card-body">
                        <caption>
                            <h2 style="font-family: 'Bogle'; font-size: 40px;">New Sale</h2>
                            <hr style="height: 5px; background-color: #007DC6;">
                        </caption>
                        <fieldset class="form-group" style="width: 250px; margin-left: 500px;">
                            <label>Customer Name</label>
                            <select name="product" value='${product.name}' />" 
                            <c:forEach items="${dropProduct}" var="product">
                                <option value="${product.name}">${product.name}</option>
                            </c:forEach>
                            </select>
                        </fieldset>
                        <fieldset class="form-group" style="width: 250px; margin-left: 500px;">
                            <label>Payment Method</label>
                            <select name="category" value='${product.category}' />" 
                            <c:forEach items="${dropCategory}" var="category">
                                <option value="${category.name}">${category.name}</option>
                            </c:forEach>
                            </select>
                        </fieldset>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Unit Price</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <select id = "selecter" name="product" value='${product.name}'  onchange="displayStuff()" />" 
                                        <option selected> Select a Product</option>
                                            <c:forEach items="${dropProduct}" var="product">
                                                <option value="${product.name}">${product.name}</option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="hidden" id="one" name="product" value='${product.price}'>
                                            <c:forEach items="${dropProduct}" var="product">
                                                <option value="${product.price}">${product.price}</option>
                                            </c:forEach>
                                        </p>
                                    </td>
                                    <td><c:out value="${customer.email}" /></td>
                                    <td><c:out value="${customer.phone}" /></td>
                                </tr>
                            </tbody>
                    </table>
                </div>
            </div>
            <br>
            <div class="container text-right">
                    <a href="<%=request.getContextPath()%>/customer-new" class="btn btn-success">Sale</a>                            
            </div>
        </div>
</body>
</html>


