<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Gualmarsh / Charts</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="style">
        <link rel="stylesheet" href="css-font">

    </head>
</head>
<body>
    <div class="wrapper d-flex align-items-stretch" style="font-family: 'Bogle';">
        <nav id="sidebar">
            <div class="custom-menu">
            </div>
            <div class="p-4">
                <img src="logo" width="275" height="65" alt="Logo"/> 
                <ul class="list-unstyled components mb-5">
                    <li>
                        <a href="<%=request.getContextPath()%>/home"><span class="fa fa-home mr-3"></span> Home</a>
                    </li>
                    <li class="active">
                        <a href="<%=request.getContextPath()%>/charts"><span class="fa fa-bar-chart mr-3"></span> Charts</a>
                    </li>
                    <li>
                        <a href="<%=request.getContextPath()%>/employees"><span class="fa fa-users mr-3"></span> Employees</a>
                    </li>
                    <li>
                        <a href="<%=request.getContextPath()%>/customers"><span class="fa fa-user-o mr-3"></span> Customers</a>
                    </li>
                    <li>
                        <a href="<%=request.getContextPath()%>/products"><span class="fa fa-shopping-cart mr-3"></span> Products</a>
                    </li>
                    <li>
                        <a href="<%=request.getContextPath()%>/sale"><span class="fa fa-usd mr-3"></span> New Sale</a>
                    </li>
                    <li>
                        <a href="<%=request.getContextPath()%>/report-list"><span class="fa fa-file-text mr-3"></span> Reports</a>
                    </li>
                </ul>

                <div class="footer">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;
                        <script>document.write(new Date().getFullYear());</script> All rights reserved | 
                            <i class="icon-heart" aria-hidden="true"></i>GADAFA <a
                            href="https://fidelitas.com" target="_blank"></a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>

            </div>
        </nav>

        <!-- Charts -->
        <div id="content" class="p-4 p-md-5 pt-5">
            <div class="row">
                <div class="container">
                    <h3 class="text-center" style="font-family: 'Bogle'; font-size: 40px;">Charts</h3>
                    <hr style="height: 5px; background-color: #007DC6;">                       
                    <div class="containericon" style="top: 200px; left: 600px">
                    <img src="pieicon" alt="Pie Chart" class="image" style="top: 200px; left: 600px; width: 100%;"/>
                    <div class="middle" style="  top: 115%; left: 50%;">
                        <div> 
                            <a href="<%=request.getContextPath()%>/piechart" class="btn btn-success">Pie Chart</a>
                        </div>                       
                    </div>
                    <div class="containericon" style="top: 15px; left: 750px">
                    <img src="lineicon" alt="Line Chart" class="image" style="top: 15px; left: 750px; width: 100%;"/>
                    <div class="middle" style="  top: 115%; left: 50%;">
                        <div> 
                            <a href="<%=request.getContextPath()%>/simplelinechart" class="btn btn-success">Line Chart</a>
                        </div>                       
                    </div>
                </div>
                </div>
                </div>
            </div>

        </div>
</body>
</html>
