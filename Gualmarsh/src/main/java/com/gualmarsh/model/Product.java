package com.gualmarsh.model;


public class Product {
    
    protected int id;
    protected String name;
    protected String price;
    protected String description;
    protected String last_update;
    protected String category;

    public Product() {
    }

    public Product(String name, String price, String description, String last_update, String category) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.last_update = last_update;
        this.category = category;
    }

    public Product(int id, String name, String price, String description, String last_update, String category) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.last_update = last_update;
        this.category = category;
    }

    public Product(String name, String price) {
        this.name = name;
        this.price = price;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    
    
}
