package com.gualmarsh.model;

public class Employee {
    protected int id;
    protected String name;
    protected int dni;
    protected String email;
    protected String phone;
    protected String access_level;
    protected String username;
    protected String password;

    public Employee() {
    }

    public Employee(String name, int dni, String username, String password, String email, String phone, String access_level) {
        this.name = name;
        this.dni = dni;
        this.username = username;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.access_level = access_level;
    }

    public Employee(int id, String name, int dni, String username, String password, String email, String phone, String access_level) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.dni = dni;
        this.email = email;
        this.phone = phone;
        this.access_level = access_level;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAccess_level() {
        return access_level;
    }

    public void setAccess_level(String access_level) {
        this.access_level = access_level;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
