
package com.gualmarsh.model;


public class Customer {
    protected int id;
    protected String name;
    protected String email;
    protected String phone;
    protected String address;
    protected String city_region;
    protected String cc_number;

    public Customer() {
    }

    public Customer(String name, String email, String phone, String address, String city_region, String cc_number) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.city_region = city_region;
        this.cc_number = cc_number;
    }

    public Customer(int id, String name, String email, String phone, String address, String city_region, String cc_number) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.city_region = city_region;
        this.cc_number = cc_number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity_region() {
        return city_region;
    }

    public void setCity_region(String city_region) {
        this.city_region = city_region;
    }

    public String getCc_number() {
        return cc_number;
    }

    public void setCc_number(String cc_number) {
        this.cc_number = cc_number;
    }
    
    
}
