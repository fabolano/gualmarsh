package com.gualmarsh.model;

public class Order extends Product{
    
    protected int quantity;
    protected int id;
    protected String payment;
    protected String date;

    public Order() {
    }

    public Order(int quantity, String payment, String date, String name, String price) {
        super(name, price);
        this.quantity = quantity;
        this.payment = payment;
        this.date = date;
    }

    public Order(int quantity, int id, String payment, String date, String name, String price) {
        super(name, price);
        this.quantity = quantity;
        this.id = id;
        this.payment = payment;
        this.date = date;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    
    
}
