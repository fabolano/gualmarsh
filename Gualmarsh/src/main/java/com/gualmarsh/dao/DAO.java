package com.gualmarsh.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.gualmarsh.model.Product;
import com.gualmarsh.model.Login;
import com.gualmarsh.model.Category;
import com.gualmarsh.model.Customer;
import com.gualmarsh.model.Employee;

public class DAO {
    
    private final String jdbcURL = "jdbc:mysql://34.73.110.23:3306/UF_116650693_DB?useSSL=false&allowPublicKeyRetrieval=true";
    private final String jdbcUsername = "UF_116650693_DBA";
    private final String jdbcPassword = "vivasaprissa";
    private static final String SELECT_LOGIN_BY_USERNAME_ID = "select * from user where username = ? and password = ?";
    private static final String INSERT_PRODUCTS_SQL = "INSERT INTO product" + "  (name, price, description, last_update, category) VALUES " + " (?, ?, ?, ?, ?);";
    private static final String INSERT_CATEGORIES_SQL = "INSERT INTO category" + "  (name, id) VALUES " + " (?, ?);";
    private static final String INSERT_CUSTOMERS_SQL = "INSERT INTO customer" + "  (name, email, phone, address, city_region, cc_number) VALUES " + " (?, ?, ?, ?, ?, ?);";
    private static final String INSERT_EMPLOYEES_SQL = "INSERT INTO user" + "  (name, dni, username, password, email, phone, access_level) VALUES " + " (?, ?, ?, ?, ?, ?, ?);";
    private static final String SELECT_PRODUCT_BY_ID = "select id,name,price,description,last_update,category from product where id =?";
    private static final String SELECT_CATEGORY_BY_ID = "select id,name from category where id =?";
    private static final String SELECT_CATEGORY_DROPDOWN = "select * from category order by name";
    private static final String SELECT_PRODUCT_DROPDOWN = "select * from product order by name";
    private static final String SELECT_CUSTOMER_BY_ID = "select id,name,email,phone,address,city_region,cc_number from customer where id =?";
    private static final String SELECT_EMPLOYEE_BY_ID = "select id,name,dni,username,password,email,phone,access_level from user where id =?";
    private static final String SELECT_ALL_PRODUCTS = "select * from product";
    private static final String SELECT_ALL_CATEGORIES = "select * from category";
    private static final String SELECT_ALL_CUSTOMERS = "select * from customer";
    private static final String SELECT_ALL_EMPLOYEES = "select * from user";
    private static final String DELETE_PRODUCTS_SQL = "delete from product where id = ?;";
    private static final String DELETE_CATEGORIES_SQL = "delete from category where id = ?;";
    private static final String DELETE_CUSTOMERS_SQL = "delete from customer where id = ?;";
    private static final String DELETE_EMPLOYEES_SQL = "delete from user where id = ?;";
    private static final String UPDATE_PRODUCTS_SQL = "update product set name = ?,price= ?, description =?, last_update =?, category =? where id = ?;";
    private static final String UPDATE_CATEGORIES_SQL = "update category set name = ? where id = ?;";
    private static final String UPDATE_CUSTOMERS_SQL = "update customer set name = ?,email= ?, phone =?, address =?, city_region =?, cc_number =? where id = ?;";
    private static final String UPDATE_EMPLOYEES_SQL = "update user set name = ?, dni= ?, username = ?, password =?, email =?, phone =?, access_level =? where id = ?;";
    
    public Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }
    public boolean validate(Login loginBean) throws ClassNotFoundException {
        boolean status = false;
         try (Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_LOGIN_BY_USERNAME_ID)) {
            preparedStatement.setString(1, loginBean.getUsername());
            preparedStatement.setString(2, loginBean.getPassword());
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while(rs.next()){
                String access = rs.getString("access_level");
                System.out.println(access);
                loginBean.setAccess(access);
                status=true;
            }
            
        } catch (SQLException e) {
            printSQLException(e);
        }
        return status;
    }
    public void insertProduct(Product product) throws SQLException {
        System.out.println(INSERT_PRODUCTS_SQL);
        try (Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(INSERT_PRODUCTS_SQL)) {
            preparedStatement.setString(1, product.getName());
            preparedStatement.setString(2, product.getPrice());
            preparedStatement.setString(3, product.getDescription());
            preparedStatement.setString(4, product.getLast_update());
            preparedStatement.setString(5, product.getCategory());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }
    public void insertEmployee(Employee employee) throws SQLException {
        System.out.println(INSERT_EMPLOYEES_SQL);
        try (Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(INSERT_EMPLOYEES_SQL)) {
            preparedStatement.setString(1, employee.getName());
            preparedStatement.setInt(2, employee.getDni());
            preparedStatement.setString(3, employee.getUsername());
            preparedStatement.setString(4, employee.getPassword());
            preparedStatement.setString(5, employee.getEmail());
            preparedStatement.setString(6, employee.getPhone());
            preparedStatement.setString(7, employee.getAccess_level());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }
    public void insertCategory(Category category) throws SQLException {
        System.out.println(INSERT_CATEGORIES_SQL);
        try (Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CATEGORIES_SQL)) {
            preparedStatement.setString(1, category.getName());
            preparedStatement.setInt(2, category.getId());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }
    public void insertCustomer(Customer customer) throws SQLException {
        System.out.println(INSERT_CUSTOMERS_SQL);
        try (Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CUSTOMERS_SQL)) {
            preparedStatement.setString(1, customer.getName());
            preparedStatement.setString(2, customer.getEmail());
            preparedStatement.setString(3, customer.getPhone());
            preparedStatement.setString(4, customer.getAddress());
            preparedStatement.setString(5, customer.getCity_region());
            preparedStatement.setString(6, customer.getCc_number());
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }
    public Product selectProduct(int id) {
        Product product = null;
        try (Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_PRODUCT_BY_ID);) {
            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                String name = rs.getString("name");
                String price = rs.getString("price");
                String description = rs.getString("description");
                String last_update = rs.getString("last_update");
                String category = rs.getString("category");
                product = new Product(id, name, price, description, last_update, category);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return product;
    }
    public Category selectCategory(int id) {
        Category category = null;
        try (Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_CATEGORY_BY_ID);) {
            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                String name = rs.getString("name");
                category = new Category(id, name);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return category;
    }
    public Customer selectCustomer(int id) {
        Customer customer = null;
        try (Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_CUSTOMER_BY_ID);) {
            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                String name = rs.getString("name");
                String email = rs.getString("email");
                String phone = rs.getString("phone");
                String address = rs.getString("address");
                String city_region = rs.getString("city_region");
                String cc_number = rs.getString("cc_number");
                customer = new Customer(id, name, email, phone, address, city_region, cc_number);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return customer;
    }
    public Employee selectEmployee(int id) {
        Employee employee = null;
        try (Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_EMPLOYEE_BY_ID);) {
            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                String name = rs.getString("name");
                int dni = rs.getInt("dni");
                String username = rs.getString("username");
                String password = rs.getString("password");
                String email = rs.getString("email");
                String phone = rs.getString("phone");
                String access_level = rs.getString("access_level");
                employee = new Employee(id, name, dni, username, password, email, phone, access_level);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return employee;
    }
    public List<Product> selectAllProducts() {
        List<Product> products = new ArrayList<>();
        try (Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_PRODUCTS);) {
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String price = rs.getString("price");
                String description = rs.getString("description");
                String last_update = rs.getString("last_update");
                String category = rs.getString("category");
                products.add(new Product(id, name, price, description, last_update, category));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return products;
    }
    public List<Category> selectAllCategories() {
        List<Category> categories = new ArrayList<>();
        try (Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_CATEGORIES);) {
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                categories.add(new Category(id, name));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return categories;
    }
    public List<Category> selectCategoryDropdown() throws SQLException {
        List<Category> listCategory = new ArrayList<>();
       
        try (Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_CATEGORY_DROPDOWN);) {
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
             
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                Category category = new Category(id, name);
                listCategory.add(category);
            }          
             
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        }      
         
        return listCategory;
    }
    public List<Product> selectProductDropdown() throws SQLException {
        List<Product> listProduct = new ArrayList<>();
       
        try (Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_PRODUCT_DROPDOWN);) {
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
             
            while (rs.next()) {
                String name = rs.getString("name");
                String price = rs.getString("price");
                Product product = new Product(name, price);
                listProduct.add(product);
            }          
             
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        }      
         
        return listProduct;
    }
    public List<Customer> selectAllCustomers() {
        List<Customer> customers = new ArrayList<>();
        try (Connection connection = getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_CUSTOMERS);) {
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String email = rs.getString("email");
                String phone = rs.getString("phone");
                String address = rs.getString("address");
                String city_region = rs.getString("city_region");
                String cc_number = rs.getString("cc_number");
                customers.add(new Customer(id, name, email, phone, address, city_region, cc_number));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return customers;
    }
    public List<Employee> selectAllEmployees() {
        List<Employee> employees = new ArrayList<>();
        try (Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_EMPLOYEES);) {
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int dni = rs.getInt("dni");
                String username = rs.getString("username");
                String password = rs.getString("password");
                String email = rs.getString("email");
                String phone = rs.getString("phone");
                String access_level = rs.getString("access_level");
                employees.add(new Employee(id, name, dni, username, password, email, phone, access_level));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return employees;
    }
    public boolean deleteProduct(int id) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = getConnection();
                PreparedStatement statement = connection.prepareStatement(DELETE_PRODUCTS_SQL);) {
            statement.setInt(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }
    public boolean deleteCategory(int id) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = getConnection();
                PreparedStatement statement = connection.prepareStatement(DELETE_CATEGORIES_SQL);) {
            statement.setInt(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }
    public boolean deleteCustomer(int id) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = getConnection();
                PreparedStatement statement = connection.prepareStatement(DELETE_CUSTOMERS_SQL);) {
            statement.setInt(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }
    public boolean deleteEmployee(int id) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = getConnection();
                PreparedStatement statement = connection.prepareStatement(DELETE_EMPLOYEES_SQL);) {
            statement.setInt(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        }
        return rowDeleted;
    }
    public boolean updateProduct(Product product) throws SQLException {
        boolean rowUpdated;
        try (Connection connection = getConnection();
                PreparedStatement statement = connection.prepareStatement(UPDATE_PRODUCTS_SQL);) {
            statement.setString(1, product.getName());
            statement.setString(2, product.getPrice());
            statement.setString(3, product.getDescription());
            statement.setString(4, product.getLast_update());
            statement.setString(5, product.getCategory());
            statement.setInt(6, product.getId());

            rowUpdated = statement.executeUpdate() > 0;
        }
        return rowUpdated;
    }
    public boolean updateCategory(Category category) throws SQLException {
        boolean rowUpdated;
        try (Connection connection = getConnection();
                PreparedStatement statement = connection.prepareStatement(UPDATE_CATEGORIES_SQL);) {
            statement.setString(1, category.getName());
            statement.setInt(2, category.getId());

            rowUpdated = statement.executeUpdate() > 1;
        }
        return rowUpdated;
    }
    public boolean updateCustomer(Customer customer) throws SQLException {
        boolean rowUpdated;
        try (Connection connection = getConnection();
                PreparedStatement statement = connection.prepareStatement(UPDATE_CUSTOMERS_SQL);) {
            statement.setString(1, customer.getName());
            statement.setString(2, customer.getEmail());
            statement.setString(3, customer.getPhone());
            statement.setString(4, customer.getAddress());
            statement.setString(5, customer.getCity_region());
            statement.setString(6, customer.getCc_number());
            statement.setInt(7, customer.getId());

            rowUpdated = statement.executeUpdate() > 0;
        }
        return rowUpdated;
    }
    public boolean updateEmployee(Employee employee) throws SQLException {
        boolean rowUpdated;
        try (Connection connection = getConnection();
                PreparedStatement statement = connection.prepareStatement(UPDATE_EMPLOYEES_SQL);) {
            statement.setString(1, employee.getName());
            statement.setInt(2, employee.getDni());
            statement.setString(3, employee.getUsername());
            statement.setString(4, employee.getPassword());
            statement.setString(5, employee.getEmail());
            statement.setString(6, employee.getPhone());
            statement.setString(7, employee.getAccess_level());
            statement.setInt(8, employee.getId());

            rowUpdated = statement.executeUpdate() > 0;
        }
        return rowUpdated;
    }
    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

}
