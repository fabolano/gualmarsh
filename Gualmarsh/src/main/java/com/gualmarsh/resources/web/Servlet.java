package com.gualmarsh.resources.web;

import com.gualmarsh.dao.DAO;
import com.gualmarsh.model.Category;
import com.gualmarsh.model.Customer;
import com.gualmarsh.model.Login;
import com.gualmarsh.model.Product;
import com.gualmarsh.model.Employee;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@WebServlet("/")
public class Servlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private DAO DAO;
    public int customId;
    
    public void init() {
        DAO = new DAO();
        
    }
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        Login loginBean = new Login();
        loginBean.setUsername(username);
        loginBean.setPassword(password);
        if (username!= null && password!=null){
            try {
                if (DAO.validate(loginBean) && ("admin".equals(loginBean.getAccess()))) {
                       
                    response.sendRedirect("home");
                    
                } else if (DAO.validate(loginBean) && ("cashier".equals(loginBean.getAccess()))) {
               
                    response.sendRedirect("sale");
                }else {
                     response.sendRedirect("login");
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }else{
            doGet(request,response);
        }
   }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();
        try {
            switch (action) {
                case "/product-new":
                    newProduct(request, response, "product/form.jsp");
                    break;
                case "/category-new":
                    httpDispatcher(request, response, "category/form.jsp");
                    break;
                case "/customer-new":
                    httpDispatcher(request, response, "customer/form.jsp");
                    break;
                case "/employee-new":
                    httpDispatcher(request, response, "employee/form.jsp");
                    break;    
                case "/product-insert":
                    insertProduct(request, response);
                    break;
                case "/category-insert":
                    insertCategory(request, response);
                    break;
                case "/customer-insert":
                    insertCustomer(request, response);
                    break;
                case "/employee-insert":
                    insertEmployee(request, response);
                    break;    
                case "/product-delete":
                    deleteProduct(request, response);
                    break;
                case "/category-delete":
                    deleteCategory(request, response);
                    break;
                case "/customer-delete":
                    deleteCustomer(request, response);
                    break;
                case "/employee-delete":
                    deleteEmployee(request, response);
                    break;    
                case "/product-edit":
                    showEditProdForm(request, response, "product/form.jsp");
                    break;
                case "/category-edit":
                    showEditCatForm(request, response, "category/form.jsp");
                    break;
                case "/customer-edit":
                    showEditCustForm(request, response, "customer/form.jsp");
                    break;
                case "/employee-edit":
                    showEditEmpForm(request, response, "employee/form.jsp");
                    break;    
                case "/product-update":
                    updateProduct(request, response);
                    break;
                case "/category-update":
                    updateCategory(request, response);
                    break;
                case "/customer-update":
                    updateCustomer(request, response);
                    break;  
                case "/employee-update":
                    updateEmployee(request, response);
                    break;           
                case "/products":
                    listProducts(request, response, "product/list.jsp");
                    break;
                case "/categories":
                    listCategories(request, response, "category/list.jsp");
                    break;   
                case "/customers":
                    listCustomers(request, response, "customer/list.jsp");
                    break;
                case "/employees":
                    listEmployees(request, response, "employee/list.jsp");
                    break;     
                case "/sale":
                    saleForm(request, response, "order/sale.jsp");
                    break; 
                case "/logo":
                    contentLoader(request, response, "resources/images/logo.png");
                    break; 
                case "/employeeicon":
                    contentLoader(request, response, "resources/images/employee.png");
                    break; 
                case "/customericon":
                    contentLoader(request, response, "resources/images/customer.png");
                    break; 
                case "/producticon":
                    contentLoader(request, response, "resources/images/product.png");
                    break; 
                case "/saleicon":
                    contentLoader(request, response, "resources/images/sale.png");
                    break; 
                case "/reporticon":
                    contentLoader(request, response, "resources/images/report.png");
                    break; 
                case "/profileicon":
                    contentLoader(request, response, "resources/images/profile.png");
                    break; 
                case "/graphicon":
                    contentLoader(request, response, "resources/images/graph.png");
                    break; 
                case "/logouticon":
                    contentLoader(request, response, "resources/images/logout.png");
                    break; 
                case "/prodreporticon":
                    contentLoader(request, response, "resources/images/productsreport.png");
                    break;
                case "/employeereporticon":
                    contentLoader(request, response, "resources/images/employeereport.png");
                    break;
                case "/custreporticon":
                    contentLoader(request, response, "resources/images/customerreport.png");
                    break;
               case "/pieicon":
                    contentLoader(request, response, "resources/images/piechart.png");
                    break; 
                case "/lineicon":
                    contentLoader(request, response, "resources/images/linegraph.png");
                    break; 
                case "/css-font":
                    contentLoader(request, response, "resources/css/font.css");
                    break;
                case "/bold":
                    contentLoader(request, response, "resources/font/BogleWeb-Bold.woff2");
                    break; 
                case "/regular":
                    contentLoader(request, response, "resources/font/BogleWeb-Regular.woff2");
                    break;
                case "/style":
                    contentLoader(request, response, "resources/css/style.css");
                    break;
                case "/jquery":
                    contentLoader(request, response, "resources/js/jquery.js");
                    break;
                case "/popper":
                    contentLoader(request, response, "resources/js/popper.js");
                    break;
                case "/bootstrap":
                    contentLoader(request, response, "resources/js/bootstrap.js");
                    break;
                case "/Product-Report":
                    jasperLoader(request, response, "report/Product.jasper");
                    break;
                case "/Customer-Report":
                    jasperLoader(request, response, "report/Customer.jasper");
                    break;
                case "/Employee-Report":
                    jasperLoader(request, response, "report/Employee.jasper");
                    break;
                case "/report-list":
                    httpDispatcher(request, response, "report/report.jsp");
                    break;    
                case "/main":
                    contentLoader(request, response, "resources/js/main.js");
                    break;
                case "/cscharts":
                    contentLoader(request, response, "resources/css/charts.css");
                    break;
                case "/piechartcss":
                    contentLoader(request, response, "resources/css/piechart.css");
                    break;
                case "/jscharts":
                    contentLoader(request, response, "resources/js/jscharts.js");
                    break;
                case "/jspichart":
                    contentLoader(request, response, "resources/js/jspichart.js");
                    break;  
                case "/login":
                    httpDispatcher(request, response, "login.jsp");
                    break;
                case "/home":
                    httpDispatcher(request, response, "home.jsp");
                    break;
                case "/charts":
                    httpDispatcher(request, response, "charts.jsp");
                    break;
                case "/simplelinechart":
                    httpDispatcher(request, response, "simplelinechart.jsp");
                    break;
                case "/piechart":
                    httpDispatcher(request, response, "piechart.jsp");
                    break;   
                default:
                    httpDispatcher(request, response, "redirect.jsp");
                    break;    
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        } catch (JRException ex){
            throw new ServletException(ex);
        }
    }
    private void httpDispatcher(HttpServletRequest request, HttpServletResponse response, String path)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher(path);
        dispatcher.forward(request, response);
    }
    public void contentLoader(HttpServletRequest request, HttpServletResponse response, String path)
            throws IOException {
        ServletOutputStream out;
        out = response.getOutputStream();
        String realPath = getServletContext().getRealPath(path);
        FileInputStream fin = new FileInputStream(realPath);
        BufferedInputStream bin = new BufferedInputStream(fin);
        BufferedOutputStream bout = new BufferedOutputStream(out);
        int ch = 0;;
        while ((ch = bin.read()) != -1) {
            bout.write(ch);
        }

        bin.close();
        fin.close();
        bout.close();
        out.close();
    }
    public void jasperLoader(HttpServletRequest request, HttpServletResponse response, String path)
            throws IOException, JRException {
        try {
            File jasper = new File(getServletContext().getRealPath(path));
            System.out.println(getServletContext().getRealPath(path));
            JasperPrint reporteJasper = JasperFillManager.fillReport(jasper.getPath(), null, DAO.getConnection());
            response.setContentType("application/pdf");
            response.addHeader("Content-Type", "application/pdf");
            ServletOutputStream flow = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(reporteJasper, flow);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void newProduct(HttpServletRequest request, HttpServletResponse response, String path)
            throws SQLException, IOException, ServletException {
        List<Category> dropCategory = DAO.selectCategoryDropdown();
        request.setAttribute("dropCategory", dropCategory);
        httpDispatcher(request, response, path);
    }
    private void saleForm(HttpServletRequest request, HttpServletResponse response, String path)
            throws SQLException, IOException, ServletException {
        List<Product> dropProduct = DAO.selectProductDropdown();
        request.setAttribute("dropProduct", dropProduct);
        httpDispatcher(request, response, path);
    }
    private void listProducts(HttpServletRequest request, HttpServletResponse response, String path)
            throws SQLException, IOException, ServletException {
        List<Product> listProduct = DAO.selectAllProducts();
        request.setAttribute("listProduct", listProduct);
        httpDispatcher(request, response, path);
    }
    private void listCategories(HttpServletRequest request, HttpServletResponse response, String path)
            throws SQLException, IOException, ServletException {
        List<Category> listCategory= DAO.selectAllCategories();
        request.setAttribute("listCategory", listCategory);
        httpDispatcher(request, response, path);
    }
    private void listCustomers(HttpServletRequest request, HttpServletResponse response, String path)
            throws SQLException, IOException, ServletException {
        List<Customer> listCustomer = DAO.selectAllCustomers();
        request.setAttribute("listCustomer", listCustomer);
        httpDispatcher(request, response, path);
    }
    private void listEmployees(HttpServletRequest request, HttpServletResponse response, String path)
            throws SQLException, IOException, ServletException {
        List<Employee> listEmployee = DAO.selectAllEmployees();
        request.setAttribute("listEmployee", listEmployee);
        httpDispatcher(request, response, path);
    }
    private void showEditProdForm(HttpServletRequest request, HttpServletResponse response, String path)
            throws SQLException, ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Product existingProduct = DAO.selectProduct(id);
        request.setAttribute("product", existingProduct);
        List<Category> dropCategory= DAO.selectCategoryDropdown();
        request.setAttribute("dropCategory", dropCategory);
        httpDispatcher(request, response, path);
    }
    private void showEditCatForm(HttpServletRequest request, HttpServletResponse response, String path)
            throws SQLException, ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Category existingCategory = DAO.selectCategory(id);
        request.setAttribute("category", existingCategory);
        httpDispatcher(request, response, path);    
    }
    private void showEditCustForm(HttpServletRequest request, HttpServletResponse response, String path)
            throws SQLException, ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Customer existingCustomer = DAO.selectCustomer(id);
        request.setAttribute("customer", existingCustomer);
        httpDispatcher(request, response, path);
    }
    private void showEditEmpForm(HttpServletRequest request, HttpServletResponse response, String path)
            throws SQLException, ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Employee existingEmployee = DAO.selectEmployee(id);
        request.setAttribute("employee", existingEmployee);
        httpDispatcher(request, response, path);
    }
    private void insertProduct(HttpServletRequest request, HttpServletResponse response) 
            throws SQLException, IOException {
        request.setCharacterEncoding("UTF-8");
        String name = request.getParameter("name");
        String price = request.getParameter("price");
        String description = request.getParameter("description");
        String last_update = request.getParameter("last_update");
        String category = request.getParameter("category");
        Product newProduct = new Product(name, price, description, last_update, category);
        DAO.insertProduct(newProduct);
        response.sendRedirect("products");
    }
    private void insertCategory(HttpServletRequest request, HttpServletResponse response) 
            throws SQLException, IOException {
        request.setCharacterEncoding("UTF-8");
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        Category newCategory = new Category(id, name);
        DAO.insertCategory(newCategory);
        response.sendRedirect("categories");
    }
    private void insertCustomer(HttpServletRequest request, HttpServletResponse response) 
            throws SQLException, IOException {
        request.setCharacterEncoding("UTF-8");
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String address = request.getParameter("address");
        String city_region = request.getParameter("city_region");
        String cc_number = request.getParameter("cc_number");
        Customer newCustomer = new Customer(name, email, phone, address, city_region,cc_number);
        DAO.insertCustomer(newCustomer);
        response.sendRedirect("customers");
    }
    private void insertEmployee(HttpServletRequest request, HttpServletResponse response) 
            throws SQLException, IOException {
        request.setCharacterEncoding("UTF-8");
        String name = request.getParameter("name");
        String dni = request.getParameter("dni");
        String username = request.getParameter("username1");
        String password = request.getParameter("password1");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String access_level = request.getParameter("access_level");
        System.out.println(dni);
        Employee newEmployee = new Employee(name, Integer.parseInt(dni), username, password, email, phone, access_level);
        System.out.println(newEmployee);
        System.out.println(username);
        DAO.insertEmployee(newEmployee);
        response.sendRedirect("employees");
    }
    private void updateProduct(HttpServletRequest request, HttpServletResponse response) 
            throws SQLException, IOException {
        request.setCharacterEncoding("UTF-8");
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String price = request.getParameter("price");
        String description = request.getParameter("description");
        String last_update = request.getParameter("last_update");
        String category = request.getParameter("category");
        Product book = new Product(id, name, price, description, last_update, category);
        DAO.updateProduct(book);
        response.sendRedirect("products");
    }
    private void updateCategory(HttpServletRequest request, HttpServletResponse response) 
            throws SQLException, IOException {
        request.setCharacterEncoding("UTF-8");
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        Category book = new Category(id, name);
        DAO.updateCategory(book);
        response.sendRedirect("categories");
    }
    private void updateCustomer(HttpServletRequest request, HttpServletResponse response) 
            throws SQLException, IOException {
        request.setCharacterEncoding("UTF-8");
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String address = request.getParameter("address");
        String city_region = request.getParameter("city_region");
        String cc_number = request.getParameter("cc_number");
        Customer book = new Customer(id, name, email, phone, address, city_region,cc_number);
        DAO.updateCustomer(book);
        response.sendRedirect("customers");
    }
    private void updateEmployee(HttpServletRequest request, HttpServletResponse response) 
            throws SQLException, IOException {
        request.setCharacterEncoding("UTF-8");
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        int dni = Integer.parseInt(request.getParameter("dni"));
        String username = request.getParameter("username1");
        String password = request.getParameter("password1");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String access_level = request.getParameter("access_level");
        Employee book = new Employee(id, name, dni, username, password, email, phone, access_level);
        DAO.updateEmployee(book);
        response.sendRedirect("employees");
    }
    private void deleteProduct(HttpServletRequest request, HttpServletResponse response) 
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        DAO.deleteProduct(id);
        response.sendRedirect("products");
    }
    private void deleteCategory(HttpServletRequest request, HttpServletResponse response) 
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        DAO.deleteCategory(id);
        response.sendRedirect("categories");
    }
    private void deleteCustomer(HttpServletRequest request, HttpServletResponse response) 
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        DAO.deleteCustomer(id);
        response.sendRedirect("customers");
    }
    private void deleteEmployee(HttpServletRequest request, HttpServletResponse response) 
            throws SQLException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        DAO.deleteEmployee(id);
        response.sendRedirect("employees");
    }
}

